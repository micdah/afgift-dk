var gulp		= require('gulp');
var ts			= require('gulp-typescript')
var sourcemaps	= require('gulp-sourcemaps');
var concat		= require('gulp-concat');
var uglify		= require('gulp-uglify');
var gutil		= require('gulp-util');
var del			= require('del');
var merge		= require('merge-stream');
var git			= require('git-rev');
var through		= require('through2');


var tsSrc = 'src/ts/**/*.ts';

gulp.task('app-clean', function(cb) {
	del([
		'build/js/app.min.*',
		'build/js/app/'
	], cb);
});

gulp.task('app-compile', ['app-clean'], function() {
	return gulp.src([tsSrc, 'typings/**/*.d.ts'], {'base': 'src/ts/'})
		.pipe(ts({
			sortOutput: true,
			target: "ES5",
			noExternalResolve: true,
			noImplicitAny: true,
		}, undefined, ts.reporter.fullReporter()))
		.pipe(concat('app.js'))
		.pipe(gulp.dest('build/js/app/'));
});

gulp.task('app-version', ['app-clean'], function() {
	function string_src(filename, str) {
		var src = require('stream').Readable({ objectMode: true });
		src._read = function () {
			this.push(new gutil.File({ cwd: "", base: "", path: filename, contents: new Buffer(str) }))
			this.push(null)
		};
		return src;
	}
	
	return string_src('version.js', "var $version = 'develop';")
		.pipe(through.obj(function(file, enc, cb) {
			git.short(function(str) {
				file.contents = new Buffer("var $version = '" + str + "';");
				
				cb(null, file);
			});
		}))
		.pipe(gulp.dest('build/js/app/'));
});

gulp.task('app-minify', ['app-clean', 'app-compile', 'app-version'], function() {
	return gulp.src([
			'build/js/app/**/*.js'
		], {'base': 'build/js/'})
		.pipe(sourcemaps.init())
			.pipe(uglify({
				mangle: false,
			}))
			.pipe(concat('app.min.js'))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('build/js/'));
});

gulp.task('app-watch', ['app-minify'], function() {
	gulp.watch(tsSrc, ['app-minify']);
});

gulp.task('lib-clean', function(cb) {
	del([
		'build/js/lib.min.*',
		'build/js/angular/',
		'build/js/angular-i18n/',
		'build/js/angular-ui-bootstrap/',
		'build/js/bootstrap/',
		'build/js/jquery/'
	], cb);
});

gulp.task('lib-compile', ['lib-clean'], function() {
	var jquery = gulp.src([
		'node_modules/jquery/dist/jquery.js'	
		], {'base': 'node_modules/jquery/dist/'})
		.pipe(gulp.dest('build/js/jquery/'));
	
	var bootstrap = gulp.src([
			'node_modules/bootstrap/dist/js/bootstrap.js'
		], {'base': 'node_modules/bootstrap/dist/js/'})
		.pipe(gulp.dest('build/js/bootstrap/'));
		
	var angular = gulp.src([
		'node_modules/angular/angular.js'
		], {'base': 'node_modules/angular/'})
		.pipe(gulp.dest('build/js/angular/'));
		
	var angulari18n = gulp.src([
		'node_modules/angular-i18n/angular-locale_da-dk.js'
		], {'base': 'node_modules/angular-i18n/'})
		.pipe(gulp.dest('build/js/angular-i18n/'));
		
	var angularBootstrapUi = gulp.src([
		'node_modules/angular-ui-bootstrap/ui-bootstrap-tpls.js'
		], {'base': 'node_modules/angular-ui-bootstrap'})
		.pipe(gulp.dest('build/js/angular-ui-bootstrap/'));
		
	return merge(jquery, bootstrap, angular, angulari18n, angularBootstrapUi);
});

gulp.task('lib-minify', ['lib-clean', 'lib-compile'], function() {
	return gulp.src([
			'build/js/jquery/**/*.js',
			'build/js/bootstrap/**/*.js',
			'build/js/angular/**/*.js',
			'build/js/angular-i18n/**/*.js',
			'build/js/angular-ui-bootstrap/**/*.js'
		], {'base': 'build/js/'})
		.pipe(sourcemaps.init())
			.pipe(uglify({
				mangle: false,
			}))
			.pipe(concat('lib.min.js'))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('build/js/'));
});

gulp.task('scripts-clean', ['app-clean', 'lib-clean']);

gulp.task('scripts-build', ['scripts-clean', 'app-compile', 'app-version', 'app-minify', 'lib-compile', 'lib-minify']);
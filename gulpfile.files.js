var gulp		= require('gulp');
var gulpif		= require('gulp-if');
var imagemin	= require('gulp-imagemin');
var del			= require('del');

var htmlSrc = ['src/**/*.html', 'src/**/*.html'];

gulp.task('html-clean', function(cb) {
	del(['build/**/*.htm', 'build/**/*.html'], cb);
});

gulp.task('html-build', ['html-clean'], function() {
	return gulp.src(htmlSrc, {'base': 'src/'})
		.pipe(gulp.dest('build/'));
});

gulp.task('html-watch', ['html-build'], function() {
	gulp.watch(htmlSrc, ['html-build']);
})

gulp.task('bootstrap-fonts-clean', function(cb) {
	del(['build/fonts/'], cb);
});

gulp.task('bootstrap-fonts-build', ['bootstrap-fonts-clean'], function() {
	return gulp.src('node_modules/bootstrap/dist/fonts/*', {'base': 'node_modules/bootstrap/dist/fonts/'})
		.pipe(gulp.dest('build/fonts/'));
});

var imgSrc = 'src/img/**/*';

gulp.task('img-clean', function(cb) {
	del(['build/img/**/*'], cb);
});

gulp.task('img-build', ['img-clean'], function() {	
	return gulp.src(imgSrc, {'base': 'src/img/'})
		.pipe(imagemin({optimizationLevel: 5}))
		.pipe(gulp.dest('build/img/'));
});

gulp.task('img-watch', ['img-build'], function() {
	gulp.watch(imgSrc, ['img-build']);
});

gulp.task('files-clean', ['html-clean', 'bootstrap-fonts-clean', 'img-clean']);

gulp.task('files-build', ['files-clean', 'html-build', 'bootstrap-fonts-build', 'img-build']);

gulp.task('files-watch', ['files-clean', 'html-watch', 'img-watch']);
/// <reference path="../_libs.ts" />
/// <reference path="IEventArgs.ts" />
/// <reference path="../services/ICalculatorService.ts" />

module afgiftCalc {
	'use strict';
	
	export interface IPageScope extends ng.IScope {
		newOrUsed: string;
		price: string;
		newPrice: string;
		SubmitForm: () => void;
		Changed: () => void;
		result: PriceResult;
	}
}
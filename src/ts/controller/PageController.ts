/// <reference path="../application.ts" />
/// <reference path="IPageScope.ts" />
/// <reference path="IEventArgs.ts" />
/// <reference path="../services/ICalculatorService.ts" />

module afgiftCalc {
	'use strict';
	
	declare var $version:string;
	
	export class PageController {
		private _scope: IPageScope;
		private _timeout: ng.ITimeoutService;
		private _calculatorService: ICalculatorService;
		public version: string;
		
		
		public static $inject: any[] =	[
			'$scope',
			'$timeout',
			'CalculatorService'
		];
		
		constructor(
			private $scope: IPageScope,
			private $timeout: ng.ITimeoutService,
			private calculatorService: ICalculatorService
		) {
			this._scope = $scope;
			this._timeout = $timeout;
			this._calculatorService = calculatorService;
			this.version = $version;
			
			this.Initialize();
		}
		
		private Initialize(): void {
			this._scope.newOrUsed = 'Used';
			this._scope.SubmitForm = () => PageController.SubmitForm(this);
			this._scope.Changed = () => PageController.Changed(this);
			
			this.ResetInput();
			this.ResetResult();
		}
		
		private ResetInput(): void {
			this._scope.price = null;
			this._scope.newPrice = null;
		}
		
		private ResetResult(): void {
			this._scope.result = null;
		}
		
		public static SubmitForm(ctrl: PageController): void {
			switch (ctrl._scope.newOrUsed) {
				case 'Used':
					ctrl.CalculateUsed();
					break;
				case 'New':
					ctrl.CalculateNew();
					break;
			}
		}
		
		public static Changed(ctrl: PageController): void {
			ctrl.ResetInput();
			ctrl.ResetResult();
		}
		
		private SanitizeInput(input: string): number {
			if (input) {
				var cleanedInput = input.replace(/\./g, '').replace(/,/g, '.');
				return Number(cleanedInput);
			}
			return NaN;
		}
		
		private CalculateUsed(): void {
			var price = this.SanitizeInput(this._scope.price);
			var newPrice = this.SanitizeInput(this._scope.newPrice);
			
			if (!isNaN(price) && !isNaN(newPrice)) {
				var result = this._calculatorService.newUsedPrice(newPrice, price, Tax.Current, Tax.Proposed);
				this._scope.result = result;
			} else {
				this._scope.result = null;
			}
		}
		
		private CalculateNew(): void {
			var price = this.SanitizeInput(this._scope.price);
			
			if (!isNaN(price)) {
				var result = this._calculatorService.newPrice(price, Tax.Current, Tax.Proposed);
				this._scope.result = result;
			} else {
				this._scope.result = null;
			}
		}
		
		
	}
	
	// Register controller in application
	Application.Current().RegisterController('PageController', PageController);
}
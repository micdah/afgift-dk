/// <reference path="../application.ts" />
/// <reference path="ICalculatorService.ts" />

module afgiftCalc {
	export class CalculatorService implements ICalculatorService {
		priceIncludingTax(price: number, tax: Tax): number {
			var bottomTax = Math.min(tax.bottomTaxLimit, price) * tax.bottomTaxRate;
			var topTax = Math.max(0, price - tax.bottomTaxLimit) * tax.topTaxRate;
			
			return price + bottomTax + topTax;
		}
		
		priceExcludingTax(price: number, tax: Tax): number {
			var bottomTax = Math.min(tax.bottomTaxLimit * (1+tax.bottomTaxRate), price) * tax.bottomTaxRate / (1+tax.bottomTaxRate);
			var topTax = Math.max(0, price - tax.bottomTaxLimit*(1+tax.bottomTaxRate)) * tax.topTaxRate / (1+tax.topTaxRate);
			
			return price - bottomTax - topTax;;
		}
		
		newPrice(price: number, oldTax: Tax, newTax: Tax): PriceResult {
			var priceExludingTax = this.priceExcludingTax(price, oldTax);
			var newPrice =  this.priceIncludingTax(priceExludingTax, newTax);
			var reduction = price - newPrice;
			var reductionPercent = reduction / price * 100;
			
			return new PriceResult(newPrice, reduction, reductionPercent);
		}
		
		newUsedPrice(priceNew: number, priceUsed: number, oldTax: Tax, newTax: Tax): PriceResult {
			var usedPriceReductionPercent = priceUsed / priceNew;		
			var newPriceExludingOldTax = this.priceExcludingTax(priceNew, oldTax);
			var newPriceIncludingNewTax = this.priceIncludingTax(newPriceExludingOldTax, newTax);
			var newUsedPrice = newPriceIncludingNewTax * usedPriceReductionPercent;
			var reduction = priceUsed - newUsedPrice;
			var reductionPercent = reduction / priceUsed * 100;
			
			return new PriceResult(newUsedPrice, reduction, reductionPercent);
		}
	}
	
	// Register service with application
	Application.Current().RegisterService('CalculatorService', CalculatorService);
}
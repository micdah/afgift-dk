/// <reference path="../application.ts" />

module afgiftCalc {
	export interface ICalculatorService {
		/**
		 * Calculate price including tax based on the given tax rates
		 * 
		 * @param {number} price Price exluding any tax
		 * @param {Tax} tax Tax rates to calculate based upon
		 * @returns {number} Price including tax based on given rates
		 */
		priceIncludingTax(price: number, tax: Tax): number;
		
		/**
		 * Calculate the price exluding tax based on the given tax rates
		 * 
		 * @param {number} price Price including tax
		 * @parm {Tax} Tax rates included in price
		 * @returns {number} Price exluding tax based on given rates
		 */
		priceExcludingTax(price: number, tax: Tax): number;
		
		/**
		 * Calculate new price based on old and new tax rates
		 * 
		 * @param {number} price Price including tax based on old rates
		 * @param {Tax} oldTax Old tax rates
		 * @param {Tax} newTax New tax rates
		 * @returns {NewPrice} New price based on new tax rates
		 */
		newPrice(price: number, oldTax: Tax, newTax: Tax): PriceResult;
		
		/**
		 * Calculate new used price, based on current new/used pricings as 
		 * well as on the old and new tax rates.
		 * 
		 * @param {number} priceNew Price for item as new including old tax rates
		 * @param {number} priceused Price for the item as used including old tax rates
		 * @param {Tax} oldTax Old tax rates
		 * @param {Tax} newTax New txa rates
		 * @returns {NewUsedPrice} New used price based on new tax rates
		 */
		newUsedPrice(priceNew: number, priceUsed: number, oldTax: Tax, newTax: Tax): PriceResult;
	}
	
	export class PriceResult {
		price: number;
		reduction: number;
		reductionPercent: number;
		
		constructor(price: number, reduction: number, reductionPercent: number) {
			this.price = price;
			this.reduction = reduction;
			this.reductionPercent = reductionPercent;
		}
	}

	export class Tax {
		bottomTaxRate: number;
		topTaxRate: number;
		bottomTaxLimit: number;

		constructor(bottomTaxRate: number, topTaxRate: number, bottomTaxLimit: number) {
			this.bottomTaxRate = bottomTaxRate;
			this.topTaxRate = topTaxRate;
			this.bottomTaxLimit = bottomTaxLimit;
		}

		static Current: Tax = new Tax(1.05, 1.8, 81700);
		static Proposed: Tax = new Tax(1.05, 1.5, 82800);
	}
}